"use strict"

//js не поддерижвает асихронность, но её можно имитировать с помощью промисов и эвент лупа браузера
//каждый фрагмент кода, который должен вернуть промис, браузер будет отправлять в эвент луп и выполнять его в момент выполнения промиса

//насколько я понял async возвращает промис для эвент лупа, внутри которого промис (например из фетча), с который уже обработали мы своим кодом
//но я хз, Нолану реально надо выучить js

async function getFromIp(btnId) {
    const ip = await fetch("https://api.ipify.org/?format=json")
        .then(r => r.json()),
        btn = document.getElementById(btnId);

    const toxicUser = new InternetUser(ip, "info");

    btn.addEventListener("click", () => {
        toxicUser.getInfo()
    })
}

getFromIp("btn");

class InternetUser {
    constructor({ip}, containerId) {
        this.ip = ip;
        this.elements = {
            container: document.getElementById(containerId),
            continent: document.createElement("p"),
            country: document.createElement("p"),
            region: document.createElement("p"),
            city: document.createElement("p"),
            district: document.createElement("p")
        }
    }

    async getInfo() {
        const info = await fetch(`http://ip-api.com/json/${this.ip}?fields=continent,country,regionName,city,district`)
            .then(r => r.json())

        this.elements.continent.textContent = info.continent;
        this.elements.country.textContent = info.country;
        this.elements.region.textContent = info.regionName;
        this.elements.city.textContent = info.city;
        this.elements.district.textContent = info.district;

        this.elements.container.append(this.elements.continent, this.elements.country, this.elements.region, this.elements.city, this.elements.district)
    }
}